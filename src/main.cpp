// Copyright 2016 Robert Maier, Technical University of Munich
// Copyright 2017 Vlad Golyanik, University of Kaiserslautern
#include <iostream>
#include <vector>

#include <Eigen/Dense>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <ceres/autodiff_cost_function.h>
#include <ceres/cubic_interpolation.h>
#include <ceres/loss_function.h>
#include <ceres/problem.h>
#include <ceres/rotation.h>
#include <ceres/solver.h>


#define INVALID_RESIDUAL 0.0



// ### depth constancy / depth variation constraint

class DvoErrorDepthVariation
{

    public: 
        
        // constructor
        DvoErrorDepthVariation(     const int w,                // width
                                    const int h,                // height
                                    const float* grayRef,       // reference brightness
                                    const float* depthRef,      // reference depth
                                    const float* grayCur,       // current brightness
                                    const float* depthCur,      // current depth
                                    const double* intrinics,    // intrinsic parameters
                                    const int x,                // x-coordinate, or column
                                    const int y) :              // y-coordinate, or row
                    w_(w),
                    h_(h),
                    depthRef_(depthRef),
                    depthCurrent_(depthCur),
                    grayRef_(grayRef),
                    grayCur_(grayCur),
                    intrinics_(intrinics),
                    x_(x),
                    y_(y)
    {
    }
     
     
             // destructor
    virtual ~DvoErrorDepthVariation()
    {
    }
     
     
    template <typename T>
    bool operator()(const T* const pose, T* residuals) const
    {
        
                // rotation
                T rot[3];
		rot[0] = pose[3];
		rot[1] = pose[4];
		rot[2] = pose[5];
                
		// translation
		T t[3];
		t[0] = pose[0];
		t[1] = pose[1];
		t[2] = pose[2];
        
		// camera intrinsics
		T fx = T(intrinics_[0]);
		T fy = T(intrinics_[1]);
		T cx = T(intrinics_[2]);
		T cy = T(intrinics_[3]);
                
                // get depth for pixel
		T dRef = T(depthRef_[y_*w_ + x_]);
                T dCur = T(depthCurrent_[y_*w_ + x_]); 
                
                // check if the depth values are positive
                if ( ( dRef > T(0.0) ) && ( dCur > T(0.0) ) )
                {
                    
                    // project current 3d point directly 

                    // project 2d point back into 3d using its depth
                    T x0 = (T(x_) - cx) / fx;
                    T y0 = (T(y_) - cy) / fy;
                
                    // transform reference 3d point into current frame
                    T pt3Ref[3];
                    pt3Ref[0] = x0 * dRef;
                    pt3Ref[1] = y0 * dRef;
                    pt3Ref[2] = dRef;
                    
                    // rotate
                    T pt3Cur[3];
                    ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
                    
                    // translate
                    pt3Cur[0] += t[0];
                    pt3Cur[1] += t[1];
                    pt3Cur[2] += t[2];
                    
                    
                    if (pt3Cur[2] > T(0.0))
                    {
                                // project 3d point to 2d  
                                // pinhole camera projection 
                                T px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
                                T py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
                                
                        if (px >= T(0.0) && py >= T(0.0) && px <= T(w_-1) && py <= T(h_-1))
                        {
                            // lookup interpolated DEPTH (automatic differentiation)
                            typedef ceres::Grid2D<float, 1> ImageDataType;
                            
                            ImageDataType array(depthCurrent_, 0, h_, 0, w_);
                            
                            ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                            
                            T valCur = T(0.0);
                            
                            interpolator.Evaluate(py, px, &valCur);
                            
                            if (valCur >= T(0.0))
                            {
                                // compute depth variation residual
                                T valRef = T(depthRef_[y_*w_ + x_]);
                                residuals[0] = valRef - valCur;
                                return true;
                            }
                        }
                    }  
                }
        
                // invalid depth lookup
                residuals[0] = T(INVALID_RESIDUAL);
                return true;
        
    }
    
    
    private: 
        
        const int w_;
        const int h_;
        const float* depthRef_;
        const float* depthCurrent_;
        const float* grayRef_;
        const float* grayCur_;
        const double* intrinics_;
        const int x_;
        const int y_;
        
};




// ### Brightness constancy

class DvoErrorPhotometric
{
public:
	DvoErrorPhotometric(const int w, const int h,
						const float* grayRef, const float* depthRef,
						const float* grayCur, const float* depthCur,
						const double* intrinics, const int x, const int y) :
		w_(w),
		h_(h),
		depthRef_(depthRef),
        depthCurrent_(depthCur),
        grayRef_(grayRef),
        grayCur_(grayCur),
		intrinics_(intrinics),
        x_(x),
        y_(y)
    {
    }
    

    virtual ~DvoErrorPhotometric()
    {
    }
    

    template <typename T>
    bool operator()(const T* const pose, T* residuals) const
    {
		// rotation
                T rot[3];
		rot[0] = pose[3];
		rot[1] = pose[4];
		rot[2] = pose[5];
		// translation
		T t[3];
		t[0] = pose[0];
		t[1] = pose[1];
		t[2] = pose[2];
        
		// camera intrinsics
		T fx = T(intrinics_[0]);
		T fy = T(intrinics_[1]);
		T cx = T(intrinics_[2]);
		T cy = T(intrinics_[3]);

        // get depth for pixel
		T dRef = T(depthRef_[y_*w_ + x_]);
        if (dRef > T(0.0))
        {
            // project 2d point back into 3d using its depth
			T x0 = (T(x_) - cx) / fx;
			T y0 = (T(y_) - cy) / fy;
            
            // transform reference 3d point into current frame
            T pt3Ref[3];
            pt3Ref[0] = x0 * dRef;
            pt3Ref[1] = y0 * dRef;
            pt3Ref[2] = dRef;
			// rotate
			T pt3Cur[3];
            ceres::AngleAxisRotatePoint(rot, pt3Ref, pt3Cur);
            // translate
            pt3Cur[0] += t[0];
            pt3Cur[1] += t[1];
            pt3Cur[2] += t[2];

            if (pt3Cur[2] > T(0.0))
            {
                // project 3d point to 2d
				T px = (fx * pt3Cur[0] / pt3Cur[2]) + cx;
				T py = (fy * pt3Cur[1] / pt3Cur[2]) + cy;
                if (px >= T(0.0) && py >= T(0.0) && px <= T(w_-1) && py <= T(h_-1))
                {
                    // lookup interpolated intensity (automatic differentiation)
                    typedef ceres::Grid2D<float, 1> ImageDataType;
					ImageDataType array(grayCur_, 0, h_, 0, w_);
                    ceres::BiCubicInterpolator<ImageDataType> interpolator(array);
                    T valCur = T(0.0);
                    interpolator.Evaluate(py, px, &valCur);
                    if (valCur >= T(0.0))
                    {
                        // compute photometric residual
						T valRef = T(grayRef_[y_*w_ + x_]);
                        residuals[0] = valRef - valCur;
                        return true;
                    }
                }
            }
        }

		// invalid lookup
		residuals[0] = T(INVALID_RESIDUAL);
        return true;
    }
    
private:
	const int w_;
	const int h_;
	const float* depthRef_;
	const float* depthCurrent_;
    const float* grayRef_;
	const float* grayCur_;
	const double* intrinics_;
	const int x_;
	const int y_;
};








cv::Mat downsampleGray(const cv::Mat &gray)
{
	cv::Mat grayDown;
	cv::pyrDown(gray, grayDown, cv::Size(gray.cols / 2, gray.rows / 2));
    return grayDown;
}


cv::Mat downsampleDepth(const cv::Mat &depth)
{
	// downscaling by averaging the depth
	int w = depth.cols / 2;
	int h = depth.rows / 2;
    cv::Mat depthDown = cv::Mat::zeros(h, w, depth.type());
    for (int y = 0; y < h; ++y)
    {
		for (int x = 0; x < w; ++x)
        {
            int cnt = 0;
            float sum = 0.0f;
			float d0 = depth.at<float>(2 * y, 2 * x);
			if (d0 > 0.0f) { sum += d0; ++cnt; }
			float d1 = depth.at<float>(2 * y, 2 * x + 1);
			if (d1 > 0.0f) { sum += d1; ++cnt; }
			float d2 = depth.at<float>(2 * y + 1, 2 * x);
			if (d2 > 0.0f) { sum += d2; ++cnt; }
			float d3 = depth.at<float>(2 * y + 1, 2 * x + 1);
			if (d3 > 0.0f) { sum += d3; ++cnt; }
            if (cnt > 0)
				depthDown.at<float>(y, x) = sum / float(cnt);
        }
    }
    return depthDown;
}


void downsample(int numPyramidLevels, std::vector<Eigen::Matrix3d> &kPyramid,
	std::vector<cv::Mat> &grayRefPyramid, std::vector<cv::Mat> &depthRefPyramid, 
	std::vector<cv::Mat> &grayCurPyramid, std::vector<cv::Mat> &depthCurPyramid)
{
	for (int i = 1; i < numPyramidLevels; ++i)
	{
		// downsample camera matrix
		Eigen::Matrix3d kDown = kPyramid[i - 1];
		kDown.topLeftCorner(2, 3) = kDown.topLeftCorner(2, 3) * 0.5;
		kPyramid.push_back(kDown);
		// downsample grayscale images
		grayRefPyramid.push_back(downsampleGray(grayRefPyramid[i - 1]));
		grayCurPyramid.push_back(downsampleGray(grayCurPyramid[i - 1]));
		// downsample depth images
		depthRefPyramid.push_back(downsampleDepth(depthRefPyramid[i - 1]));
		depthCurPyramid.push_back(downsampleDepth(depthCurPyramid[i - 1]));
	}
}


cv::Mat loadDepth(const std::string &filename)
{
	// read 16 bit depth image
	cv::Mat depth16 = cv::imread(filename, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	// convert depth to float (1.0f = 1.0m)
	cv::Mat depth;
	depth16.convertTo(depth, CV_32FC1, (1.0 / 5000.0));
	return depth;
}


void loadData(cv::Mat &grayRef, cv::Mat &grayCur, cv::Mat &depthRef, cv::Mat &depthCur, Eigen::Matrix3d &K)
{
    std::string dataFolder = std::string(APP_SOURCE_DIR) + "/data/";
	cv::Mat grayRefIn = cv::imread(dataFolder + "rgb/1305031102.275326.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayRefIn.convertTo(grayRef, CV_32FC1, 1.0f / 255.0f);
	depthRef = loadDepth(dataFolder + "depth/1305031102.262886.png");
	cv::Mat grayCurIn = cv::imread(dataFolder + "rgb/1305031102.175304.png", CV_LOAD_IMAGE_GRAYSCALE);
	grayCurIn.convertTo(grayCur, CV_32FC1, 1.0f / 255.0f);
	depthCur = loadDepth(dataFolder + "depth/1305031102.160407.png");

    // initialize intrinsic matrix
    K <<    517.3, 0.0, 318.6,
			0.0, 516.5, 255.3,
			0.0, 0.0, 1.0;
}


double computeError(ceres::Problem &problem)
{
	// evaluate problem cost
	ceres::Problem::EvaluateOptions evalOptions;
	evalOptions.apply_loss_function = true;
	evalOptions.num_threads = 8;
	double errorEval;
	problem.Evaluate(evalOptions, &errorEval, 0, 0, 0);
	return errorEval;
}


int computeNumValidResiduals(const ceres::Problem &problem, const std::vector<double*> &parameters)
{
	std::vector<ceres::ResidualBlockId> problemResidualsBlocks;
	problem.GetResidualBlocks(&problemResidualsBlocks);
	int numValidResiduals = 0;
	for (size_t i = 0; i < problemResidualsBlocks.size(); ++i)
	{
		const ceres::CostFunction* costFunction = problem.GetCostFunctionForResidualBlock(problemResidualsBlocks[i]);
		double residual;
		bool eval = costFunction->Evaluate(&(parameters[0]), &residual, 0);
		if (eval && residual != INVALID_RESIDUAL)
			++numValidResiduals;
	}
	return numValidResiduals;
}




int main(int argc, char *argv[])
{
	// parameters
	int numPyramidLevels = 4;
	int maxIterations = 20;
	int maxLvl = numPyramidLevels - 1;
	int minLvl = 0;
	bool useHuber = true;
	bool earlyBreak = true;
	bool debug = false;
	double convergenceErrorRatio = 0.995;

	// load RGB-D input data
	std::cout << "load input ..." << std::endl;
	cv::Mat grayRef, grayCur, depthRef, depthCur;
        Eigen::Matrix3d K;
        loadData(grayRef, grayCur, depthRef, depthCur, K);

        // pyramid downsampling
	std::cout << "downsample input ..." << std::endl;
	std::vector<cv::Mat> grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr;
	std::vector<Eigen::Matrix3d> kPyr;
	grayRefPyr.push_back(grayRef);
	depthRefPyr.push_back(depthRef);
	grayCurPyr.push_back(grayCur);
	depthCurPyr.push_back(depthCur);
	kPyr.push_back(K);
	downsample(numPyramidLevels, kPyr, grayRefPyr, depthRefPyr, grayCurPyr, depthCurPyr);

	// initialize pose (angle-axis and translation)
	Eigen::Matrix<double, 6, 1> pose;
	pose.setZero();
	if (debug)
	{
		// translation
		pose.topRows<3>() = Eigen::Vector3d::Zero();
		// rotation
		Eigen::Matrix3d rot = Eigen::Matrix3d::Identity();
		Eigen::AngleAxisd aa(rot);
		pose.bottomRows<3>() = aa.angle() * aa.axis();
		std::cout << "pose initial = " << pose.bottomRows<3>().transpose() << std::endl;
	}
        
        
        // ### provide segmentation of the scene
        // ### the data structure for segmentation will be vector of vectors
        
        // ### debug: split the image into two segments
        
        uint num_segments = 2; 
        
        std::vector<std::vector<uint> > segments(num_segments); 
        
        
        
        // ### fill the segments with the point indexes
        // ### now, there will be two consecutive segments
        
        // ### for every segment there will be a hierarchy of masks!
        
        uint num_points = grayRefPyr[0].rows * grayRefPyr[0].cols;
        
        std::cout << num_points << std::endl; 
        
        
        for (uint i = 0; i < num_segments; i++){
           for (uint j = (i * num_points / num_segments); j < ((i+1) * num_points / num_segments); j++) {
             segments[i].push_back(j);  
           } 
        }
        
        //std::cout << segments[0].size() << std::endl; 
        //std::cout << segments[1].size() << std::endl; 
        //std::cout << segments[0][segments[0].size() - 1] << std::endl; 
        //std::cout << segments[1][segments[1].size() - 1] << std::endl; 
        
        char c; std::cin >> c; 
        
        // ### create here masks from synthetic input (above)
        // ### later, the masks will be directly interfaced from the segmentation module
        
        
        
        // ### build a mask (cv::mat for instance)
        // ### every mask will be an individual image for every segment
        // ### std::vector<cv::Mat> // a hierarchy for every segment 
        //std::vector<std::vector<cv::Mat> > segment_masks(num_segments); // segment_masks[segment][level]
        
        
        // ### it is possible to apply the same downsampling technique as used in the optimisation process
        
        
        
        // ### save a mask ()
        
        // 
        // ### run direct dense image alignment on each of the segments independently
        // 
        
        for ( uint i = 0; i < num_segments; i++)
        {
        
        
            
        // ### pre-fetch only those points which will participate in optimization 
            
            
        // ### number of residuals == < # of points > x < # data terms > etc. 
            
        // ### build     
        
	// estimate pose using direct dense image alignment
	std::cout << "coarse-to-fine optimization ..." << std::endl;
        
        double tmr = (double)cv::getTickCount();
	for (int lvl = maxLvl; lvl >= minLvl; --lvl)
	{
		//std::cout << "   level " << lvl << "..." << std::endl;
		double tmrLvl = (double)cv::getTickCount();
		grayRef = grayRefPyr[lvl];
		depthRef = depthRefPyr[lvl];
		grayCur = grayCurPyr[lvl];
		depthCur = depthCurPyr[lvl];
		double intrinsics[4] { kPyr[lvl](0, 0), kPyr[lvl](1, 1), kPyr[lvl](0, 2), kPyr[lvl](1, 2) };

		// instantiate problem
		ceres::Problem problem;
                
                // ### loss function for the brightness constancy
		ceres::LossFunction* lossFunction = 0;
		if (useHuber)
			lossFunction = new ceres::HuberLoss(4.0 / 255.0);
                
                
                // ### a scaled loss function for the depth variance
                
                ceres::LossFunction* lossFunctionDepth = 0;
		if (useHuber)
			lossFunctionDepth = new ceres::HuberLoss(5.0 / 100.0);
                
                ceres::LossFunction* lossFunctionDepthScaled = 0;
                lossFunctionDepthScaled = new ceres::ScaledLoss(lossFunctionDepth, 0.2, ceres::DO_NOT_TAKE_OWNERSHIP); 
                
                //ceres::LossFunction* lossFunctionDepthScaled = 0;
                //lossFunctionDepth = new ceres::ComposedLoss(lossFunctionDepthScale_, lossFunction); 
                
                
		const int w = grayRef.cols;
		const int h = grayRef.rows;
                
                // ### 2x because there are 2 terms now
		const size_t numResiduals = 2 * (size_t)w * (size_t)h;
                
                
		std::vector<ceres::ResidualBlockId> residualsBlocks(numResiduals, 0);
		std::vector<double*> parameters;
		parameters.push_back(pose.data());

		double errorAvgLast = std::numeric_limits<double>::max();
                
                // ### with maxIterations, # of iterations is controlled externally, i.e.,
                // the ceres solver launches for one single iteration
		for (int itr = 0; itr < maxIterations; ++itr)
		{
			//std::cout << "   iteration " << itr << ":" << std::endl;
                
			// update residual blocks
			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
                                    
					const size_t id = (size_t)(y*w + x);
                                        
                                        // ### add brightness constancy residual
                                        
					DvoErrorPhotometric* costPhotometric = new DvoErrorPhotometric(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);
                                        
					ceres::CostFunction* costFunction = new ceres::AutoDiffCostFunction<DvoErrorPhotometric, 1, 6>(costPhotometric);
                                        
					double residual;
                                        
					bool eval = costFunction->Evaluate(&(parameters[0]), &residual, 0);
                                        
					if (eval && residual != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id])
						{
							residualsBlocks[id] = problem.AddResidualBlock(costFunction, lossFunction, pose.data());
						}
					}
					else
					{
						if (residualsBlocks[id])
						{
							problem.RemoveResidualBlock(residualsBlocks[id]);
							residualsBlocks[id] = 0;
						}
						delete costFunction;
					}
					
					
					// ### add depth variation residual
					
					const size_t id_depth = (size_t)(w*h + y*w + x);
					
					DvoErrorDepthVariation* costDepthVariation = new DvoErrorDepthVariation(w, h,
						(const float*)grayRef.data, (const float*)depthRef.data,
						(const float*)grayCur.data, (const float*)depthCur.data,
						intrinsics, x, y);
                                        
                                        ceres::CostFunction* costFunctionDepth = new ceres::AutoDiffCostFunction<DvoErrorDepthVariation, 1, 6>(costDepthVariation);
                                        
                                        double residualDepth; 
                                        
                                        bool evalDepth = costFunctionDepth->Evaluate(&(parameters[0]), &residualDepth, 0);
					
					if (evalDepth && residualDepth != INVALID_RESIDUAL)
					{
						if (!residualsBlocks[id_depth])
						{
							residualsBlocks[id_depth] = problem.AddResidualBlock(costFunctionDepth, lossFunctionDepthScaled, pose.data());
						}
					}
					else
					{
						if (residualsBlocks[id_depth])
						{
							problem.RemoveResidualBlock(residualsBlocks[id_depth]);
							residualsBlocks[id_depth] = 0;
						}
						delete costFunctionDepth;
					}
					
					
				}
				
			}
			
			int numValidResidualsBefore = problem.NumResidualBlocks();
			if (numValidResidualsBefore == 0)
				continue;

			// solve NLS optimization problem
			ceres::Solver::Options options;
                        // a single iteration, # of iterations is controlled externally in the loop
			options.max_num_iterations = 1;                              
			options.num_threads = 8;
			options.linear_solver_type = ceres::DENSE_NORMAL_CHOLESKY;
			options.minimizer_progress_to_stdout = false;
			options.logging_type = ceres::SILENT;

			ceres::Solver::Summary summary;
			ceres::Solve(options, &problem, &summary);
			if (debug)
				std::cout << summary.FullReport() << std::endl;

			int numValidResidualsAfter = computeNumValidResiduals(problem, parameters);

			double error = summary.final_cost;
			double errorAvg = error / numValidResidualsAfter;
#if 0
			std::cout << "      error " << error << std::endl;
			std::cout << "      avg error last " << errorAvgLast << std::endl;
			std::cout << "      avg error " << errorAvg << std::endl;
			std::cout << "      num valid residuals before " << numValidResidualsBefore << std::endl;
			std::cout << "      num valid residuals after " << numValidResidualsAfter << std::endl;
#endif

#if 0
			double errorEval = computeError(problem);
			//std::cout << "      error eval: " << errorEval << std::endl;
#endif

			// break if no improvement
			if (earlyBreak && errorAvg / errorAvgLast > convergenceErrorRatio)
			{
				errorAvgLast = errorAvg;
				break;
			}

			errorAvgLast = errorAvg;
		}
		
		tmrLvl = ((double)cv::getTickCount() - tmrLvl) / cv::getTickFrequency();
		std::cout << "   runtime for level " << lvl << ": " << tmrLvl << std::endl;
                
    } // direct dense image alignment done
    
    
    // post-processing
    
    tmr = ((double)cv::getTickCount() - tmr)/cv::getTickFrequency();
    std::cout << "runtime: " << tmr << std::endl;

	// final result
	std::cout << "pose final = " << pose.transpose() << std::endl;
	if (debug)
	{
		// compare with expected result (approx without Huber norm)
		Eigen::Matrix<double, 6, 1> poseExpected;
		// SE3 result
		//poseExpected << -0.0021, 0.0057, 0.0374, -0.0292, -0.0183, -0.0009;
		// angle-axis result
		//poseExpected << -0.00243882, 0.00624604, 0.0372902, -0.0292, -0.0183, -0.0009;
		//std::cout << "pose expected = " << poseExpected.transpose() << std::endl;
	}
    
    
    
    
    }
    
    // ### 
    // ### loop finished ( number of segments )
    // ### 
    
    // clean up
    cv::destroyAllWindows();
    std::cout << "direct image alignment finished." << std::endl;
    
    return 0;
}
